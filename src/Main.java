import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Main {
    public static void main(String[] args) throws Exception {
        int port = 8080;
        DatagramSocket socket = new DatagramSocket(port);
        byte maxPackageSize = 8;
        State state = new State(socket, maxPackageSize);
        System.out.printf("%s || Server start\n", state.time());
    }
}
