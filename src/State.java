import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class State {

    private int amountNotifySent;
    private int amountNotifyReceived;
    private int amountPongSent;
    private int amountPingReceived;
    private final Receiver receiver;
    private final Sender sender;
    private final ArrayList<Client> clients = new ArrayList<>();

    public State(DatagramSocket socket, byte maxPackageSize) {
        this.receiver = new Receiver(socket, maxPackageSize, this);
        this.sender = new Sender(socket);
        this.receiver.start();
        this.sender.start();
    }

    public void addClient(DatagramPacket incomingPacket) {
        Client client = new Client(incomingPacket);
        this.clients.add(client);
    }

    public int getAmountNotifySent() {
        return amountNotifySent;
    }

    public void setAmountNotifySent() {
        amountNotifySent++;
    }

    public int getAmountNotifyReceived() {
        return amountNotifyReceived;
    }

    public void setAmountNotifyReceived() {
        amountNotifyReceived++;
    }

    public int getAmountPongSent() {
        return amountPongSent;
    }

    public void setAmountPongSent() {
        amountPongSent++;
    }

    public int getAmountPingReceived() {
        return amountPingReceived;
    }

    public void setAmountPingReceived() {
        amountPingReceived++;
    }

    public void closeServer() {
        System.out.printf("%s || Server close\n", this.time());
        System.out.printf("%s\nAmount notify sent: %s\nAmount notify received: %s\nAmount pong sent: %s\nAmount ping received: %s\n", this.time(), this.getAmountNotifySent(), this.getAmountNotifyReceived(), this.getAmountPongSent(), this.getAmountPingReceived());
        System.exit(0);
    }

    public String time() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm:ss"));
    }

}
